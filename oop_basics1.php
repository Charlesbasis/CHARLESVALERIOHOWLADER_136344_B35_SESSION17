<?php
class StudentInfo{
    public $std_id="";
    public $std_name="";
    public $std_cgpa=0.00;

    public function set_std_id($std_id){
        $this->std_id=$std_id;
        //echo $this->std_id; //checking
    }
    public function set_std_name($std_name){
        $this->std_name=$std_name;
        //echo $this->std_name; //checking
    }
    public function set_std_cgpa($std_cgpa){
        $this->std_cgpa=$std_cgpa;
        //echo $this->std_cgpa; //checking
    }
    public function get_std_id(){
      return $this->std_id;
    }
    public function get_std_name(){
      return $this->std_name;
    }
    public function get_std_cgpa(){
      return $this->std_cgpa;
    }
    public function showDetails(){
        echo "Student ID: ".$this->std_id ."<br>";
        echo "Student NAME: ".$this->std_name."<br>";
        echo "Student CGPA: ".$this->std_cgpa."<br>";
    }
}
$obj=new StudentInfo; //creating object from function/method
$obj->set_std_id("1227034");
$obj->set_std_name("CHARLES");
$obj->set_std_cgpa(2.71);
//echo $obj->std_id;
//echo "ID: ".$obj->get_std_id()."<br>";
//echo "NAME: ".$obj->get_std_name()."<br>";
//echo "CGPA: ".$obj->get_std_cgpa()."<br>";
$obj->showDetails();
?>